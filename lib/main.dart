import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget myname = Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            ' นายธเนศ ตาดทรัพย์ ',
            style: TextStyle(
                color: Colors.grey[800],
                fontWeight: FontWeight.w400,
                fontFamily: 'Open Sans',
                fontSize: 20,
                shadows: [
                  Shadow(
                      color: Colors.pink, blurRadius: 10, offset: Offset(5, 5)),
                  Shadow(
                      color: Colors.blue, blurRadius: 10, offset: Offset(-5, 5))
                ]),
          )
        ],
      ),
    );
    Widget listSkill = Container(
      padding: EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Image.asset(
                'images/java.jpg',
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
              const Text('Java', style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
          Column(
            children: [
              Image.asset(
                'images/html.png',
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
              const Text('Html', style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
          Column(
            children: [
              Image.asset(
                'images/vue.png',
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
              const Text('Vue', style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
          Column(
            children: [
              Image.asset(
                'images/flutter.png',
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
              const Text('Flutter',
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
          Column(
            children: [
              Image.asset(
                'images/git.png',
                width: 50,
                height: 50,
                fit: BoxFit.cover,
              ),
              const Text('Git', style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          )
        ],
      ),
    );
    Widget history = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(
                'ประวัติส่วนตัว',
                style: TextStyle(
                    color: Colors.grey[800],
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Open Sans',
                    fontSize: 20,
                    shadows: [
                      Shadow(
                          color: Colors.greenAccent,
                          blurRadius: 10,
                          offset: Offset(5, 5)),
                      Shadow(
                          color: Colors.yellow,
                          blurRadius: 10,
                          offset: Offset(-5, 5))
                    ]),
              ),
            ],
          )
        ],
      ),
    );
    Widget textHistory = const Padding(
      padding: EdgeInsets.only(left: 32, top: 15, bottom: 20),
      child: Text(
        'ผมนายธเนศ ตาดทรัพย์ เรียนอยู่ที่มหาวิทยาลัยบูรพา '
        'คณะวิทยาการสารสนเทศ สาขาวิทยาการคอมพิวเตอร์ '
        'กำลังศึกษาอยู่ชั้นปีที่4 ตอนนี้ GPA = 2.92',
        softWrap: true,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w200,
          fontFamily: 'Open Sans',
          fontSize: 15,
        ),
      ),
    );
    Widget education = Container(
      padding: EdgeInsets.only(top: 5, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(
                'การศึกษา',
                style: TextStyle(
                    color: Colors.grey[800],
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Open Sans',
                    fontSize: 20,
                    shadows: [
                      Shadow(
                          color: Colors.greenAccent,
                          blurRadius: 10,
                          offset: Offset(5, 5)),
                      Shadow(
                          color: Colors.yellow,
                          blurRadius: 10,
                          offset: Offset(-5, 5))
                    ]),
              ),
            ],
          )
        ],
      ),
    );
    Widget detailEducation = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('2012-2018', style: TextStyle(fontWeight: FontWeight.bold)),
              Text(' '),
              Text(' '),
              Text('2018-ปัจจุบัน',
                  style: TextStyle(fontWeight: FontWeight.bold))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('โรงเรียนเบญจมราชรังสฤษฎ์',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text(' '),
              Text(' '),
              Text('มหาวิทยาลัยบูรพา',
                  style: TextStyle(fontWeight: FontWeight.bold))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                'images/brr.png',
                width: 50,
                height: 50,
                fit: BoxFit.contain,
              ),
              Text(' '),
              Image.asset(
                'images/buu.png',
                width: 40,
                height: 40,
                fit: BoxFit.contain,
              ),
            ],
          )
        ],
      ),
    );
    Widget contact = Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(
                'ช่องทางติดต่อ',
                style: TextStyle(
                    color: Colors.grey[800],
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Open Sans',
                    fontSize: 20,
                    shadows: [
                      Shadow(
                          color: Colors.greenAccent,
                          blurRadius: 10,
                          offset: Offset(5, 5)),
                      Shadow(
                          color: Colors.yellow,
                          blurRadius: 10,
                          offset: Offset(-5, 5))
                    ]),
              ),
            ],
          )
        ],
      ),
    );
    Widget textContact = Container(
      padding: EdgeInsets.only(left: 0, top: 15, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text('Facebook : Taneat Tadsab',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text('Line : taneat.',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text('Email : thanet9927@gmail.com',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              Text('Tel : 0911849927, 0971652304',
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
        ],
      ),
    );
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
          backgroundColor: Colors.lime[100],
          appBar: AppBar(
            title: const Text('My Resume'),
          ),
          body: ListView(
            children: [
              Image.asset(
                'images/myimage.jpg',
                width: 50,
                height: 300,
                fit: BoxFit.cover,
              ),
              myname,
              listSkill,
              history,
              textHistory,
              education,
              detailEducation,
              contact,
              textContact
            ],
          )),
    );
  }
}
